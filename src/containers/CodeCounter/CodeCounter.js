import React, {useState} from 'react';
import './CodeCounter.css';
import {useDispatch, useSelector} from "react-redux";

const trueCode = "4512";

const CodeCounter = () => {
    const dispatch = useDispatch();
    const counter = useSelector(state => state.codeCounter);
    const numberOfCode = useSelector(state => state.numberOfCode);
    const increaseCounter = (event) => {
        dispatch({type: 'ENTER', id: event.target.id});
    };
    const decreaseCounter = () => {
        setCheck(null);
        dispatch({type: 'REMOVE'});
    };
    const [check, setCheck] = useState();
    const checkCode = () => {

        if (numberOfCode === trueCode) {
            setCheck(true);
        } else {
            setCheck(false);
        }
    };

    return (
        <div className="container">
            <p>Введите код: {Boolean(check) ? <span className="granted">Access Granted</span> : check === false ?
                <span className="denied">Access Denied</span> : check === null ? '' : ''}</p>
            <div className="field"
                 style={{
                     background: Boolean(check) ? 'lightgreen' : check === false ? 'red' : check === null ? 'white' : 'white'
                 }}
            >{counter}</div>
            <div className="buttons">
                <button id="1" onClick={increaseCounter} className="btnSize">1</button>
                <button id="2" onClick={increaseCounter} className="btnSize">2</button>
                <button id="3" onClick={increaseCounter} className="btnSize">3</button>
                <button id="4" onClick={increaseCounter} className="btnSize">4</button>
                <button id="5" onClick={increaseCounter} className="btnSize">5</button>
                <button id="6" onClick={increaseCounter} className="btnSize">6</button>
                <button id="7" onClick={increaseCounter} className="btnSize">7</button>
                <button id="8" onClick={increaseCounter} className="btnSize">8</button>
                <button id="1" onClick={increaseCounter} className="btnSize">9</button>
                <button id="9" onClick={decreaseCounter} className="btnSize">&#8855;</button>
                <button id="0" onClick={increaseCounter} className="btnSize">0</button>
                <button onClick={checkCode}>E</button>
            </div>
        </div>
    );
};

export default CodeCounter;