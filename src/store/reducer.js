const initialState = {
    codeCounter: "",
    numberOfCode: "",
};

const reducer = (state = initialState, action) => {

    if (action.type === 'ENTER') {
        if (state.codeCounter.length >= 4) {
            return {...state}
        }
        return {...state, codeCounter: state.codeCounter + "*", numberOfCode: state.numberOfCode + action.id};

    }
    if (action.type === 'REMOVE') {
        return {...state, codeCounter: state.codeCounter.slice(0, -1), numberOfCode: state.numberOfCode.slice(0, -1)};
    }

    return state;
};
export default reducer;